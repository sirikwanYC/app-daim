import React, { Component } from 'react'
import { Modal, Icon, Button } from "antd"
import ContentAssessor from './ContentAssessor'


export default class ModalAssessor extends Component {
    constructor (props) {
        super()
    }

    state = {
        visible: false
    }

    showModal = () => {
        this.setState({
            visible: true,
        })
        console.log(this.state.visible)
    }

    handleOk = (e) => {
        console.log(e);
        this.setState({
          visible: false,
        });
      }

    handleCancel = (e) => {
        console.log(e)
        this.setState({
            visible: false,
        })
    }

    render() {
        return (
            <div className="modal-assessor">
                <Button onClick={this.showModal} className="font-small font-weight-heavy"> ผู้ประเมิน </Button>
                <Modal
                    title="ผู้ประเมิน"
                    id={`assessor${this.props.index}`}
                    centered
                    visible={this.state.visible}
                    onOk={this.handleOk}
                    onCancel={this.handleCancel}
                    width="90%"
                    className="font"
                    okText="ตกลง"
                    cancelText="ยกเลิก"
                    okButtonProps={{style: {fontSize: '29px', height: '100%', background: '#52c41a'}}}
                    cancelButtonProps={{style: {fontSize: '29px', height: '100%'}}}
                >
                    <ContentAssessor index={this.props.index} />
                </Modal>
            </div>
        )
    }
  }
