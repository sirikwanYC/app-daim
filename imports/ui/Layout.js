import React, { Component } from 'react'

export default class Layout extends Component {
    constructor(props) {
        super()
    }
    render() {
        return (
            <div className="layout">
                <div className="layout-content">
                    {this.props.children}
                </div>
            </div>
        )
    }
}