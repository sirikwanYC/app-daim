import React, { Component } from 'react'
import { Input, Button } from 'antd';
import { Redirect } from 'react-router-dom'


export default class Register extends Component {
  state = {
    redirect: false
  }
  onClickOk = () => {
    this.setState({
      redirect: true
    })
  }
  render() {
    const {redirect} = this.state
    return (
      <div className="register font font-small">
      {redirect ? <Redirect to="/assessment" />: ''}
        <div className="border-content">
          <div className="logo">
            <img src="./images/logo_MOPH.png" width="30%" />
          </div>
          <div className="header">
            <p>คู่มือ</p>
            <p>ประเมินและส่งเสริมพัฒนาการเด็กกลุ่มเสี่ยง</p>
            <p>Developmental Assessment For Intervention Manual (DAIM)</p>
          </div>
          <div className="personal-infomation">
            <div className="content">
              <div className="left">
                <p>ชื่อ - สกุล เด็ก</p>
              </div>
              <div className="right">
                <Input />
              </div>
            </div>

            <div className="content">
              <div className="left">
                <p>เลขที่บัตรประจำตัวประชาชน เด็ก</p>
              </div>
              <div className="right">
                <Input />
              </div>
            </div>
            <div className="content">
              <div className="left">
                <p>วัน เดือน ปีเกิด เด็ก</p>
              </div>
              <div className="right">
                <Input />
              </div>
            </div>
            <div className="content">
              <div className="left">
                <p>สถานที่เกิด โรงพยาบาล</p>
              </div>
              <div className="right">
                <Input />
              </div>
            </div>
            <div className="content">
              <div className="left">
                <p>น้ำหนักแรกเกิด</p>
              </div>
              <div className="right">
                <Input />
              </div>
            </div>
            <div className="content">
              <div className="left">
                <p>Apgar Score</p>
              </div>
              <div className="right">
                <Input />
              </div>
            </div>
            <div className="content">
              <div className="left">
                <p>ชื่อ - สกุล บิดา หรือมารดา หรือผู้ดูแล</p>
              </div>
              <div className="right">
                <Input />
              </div>
            </div>
            <div className="content">
              <div className="left">
                <p>โทรศัพท์</p>
              </div>
              <div className="right">
                <Input />
              </div>
            </div>
            <div className="content">
              <div className="left">
                <p>สถานที่ติดต่อ</p>
              </div>
              <div className="right">
                <Input />
              </div>
            </div>
            <div className="next-button">
              <Button style={{ fontSize: '39px' }} onClick={this.onClickOk}> ต่อไป </Button>
            </div>
          </div>
        </div>
      </div>
    )
  }
}