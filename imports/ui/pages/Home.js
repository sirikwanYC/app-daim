import { Icon } from "antd"
import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import Layout from '../Layout'

export default class Home extends Component {
  state = {
    loading: false
  }

  componentWillMount = () => {
    setTimeout(() => {
      this.setState({
        loading: true
      })
    }
    , 3000)
  }

  render() {
    const { loading } = this.state
    return (
      <div className="home">
        {loading ? <Redirect to="/register" /> : ''}
        <Layout>
        <div className="loading"> <Icon type="loading" /> </div>
          <div className="box-logo-head">
            <div className="logo-head">
              <img
                src="/images/5.png"
                width="100%"
              />
            </div>
          </div>
          <div className="box-header">
            <img
              src="/images/head.png"
              width="90%"
            />
          </div>
          <div className="box-content">
            <img
              src="/images/body.png"
              width="90%"
            />
          </div>
          <div className="box-logo-footer">
            <div className="logo-one">
              <img
                src="/images/logo_MOPH.png"
                width="80%"
              />
            </div>
          </div>
          <div className="box-footer"></div>
        </Layout>
      </div>
    )
  }
}