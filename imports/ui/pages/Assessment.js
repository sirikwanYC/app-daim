import React, { Component } from 'react'
import Navbar from '../Navbar'
import Layout from '../Layout'
import Content from '../Content'

export default class Assessment extends Component {
  state = {
    openMenu: false
  }

  openMenu = () => {
    this.setState({
      openMenu: !this.state.openMenu
    })
  }
  render() {
    const { openMenu } = this.state
    return (
      <div className="container">
        <Layout>
          <Navbar openMenu={this.openMenu} text="แรกเกิด" />
          <Content openMenu={openMenu} />
        </Layout>
      </div>
    )
  }
}