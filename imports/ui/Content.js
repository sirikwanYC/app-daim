import React, { Component } from "react"
import moment from 'moment'
import 'moment/locale/th'
import { Radio, Input, Icon } from "antd"
import ModalInfo from './ModalInfo'
import ModalAssessor from './ModalAssessor'

export default class Content extends Component {
  constructor (props) {
    super()
  }
  state = {
    content: [
      {
        question: 'ขยับเคลื่อนไหวแขนขา 2 ข้างเท่ากัน (GM)',
        date: '',
        urlVideo: 'https://www.youtube.com/watch?v=EIqa2KmwIYI',
        urlSound: 'https://soundcloud.com/kwan-sirikwan-565748504/question1'
      },
      {
        question: 'มองสบตาขณะตื่น (FM)',
        date: '',
        urlVideo: 'https://www.youtube.com/watch?v=EIqa2KmwIYI',
        urlSound: 'https://soundcloud.com/kwan-sirikwan-565748504/question1'
      },
      {
        question: 'สะดุ้งหรือเคลื่อนไหวร่างกาย เมื่อได้ยินเสียงพูดในระดับปกติ (RL)',
        date: '',
        urlVideo: 'https://www.youtube.com/watch?v=EIqa2KmwIYI'
      },
      {
        question: 'เด็กเปล่งเสียงได้ ไม่ใช่ร้องไห้ (EL)',
        date: '',
        urlVideo: 'https://www.youtube.com/watch?v=EIqa2KmwIYI'
      },
      {
        question: 'ดูดนมได้ดี (PS)',
        date: '',
        urlVideo: 'https://www.youtube.com/watch?v=EIqa2KmwIYI'
      }
    ],
    visible: false,
    index: 0,
    play: false,
  }

  onClickPlaySound = (index) => {
    const url = '../../public/sound/question1.mp3'
    this.audio = new Audio(url)
    this.setState({
      play: true,
    })
    console.log(this.audio);
    this.audio.play();
  }

  onChangeRadio = (event, index) => {
    const content = this.state.content
    content[index].date = moment(new Date()).format('ll')
    this.setState({
      content
    })
  }

  showModal = (event, index) => {
    this.setState({
      visible: true,
      index
    })
  }

  handleCancel = (e) => {
    console.log(e)
    this.setState({
      visible: false,
    })
  }

  render() {
    return (
      <div className="font font-small font-weight-heavy" style={this.props.openMenu ? {position: "fixed"}: {}}>
        {this.state.content.map((value, index) => {
          return (
            <div key={index + 1}>
              <div className="content">
                <div className="content-skill font-medium">
                  <div className="sound-icon">
                    <Icon onClick={() => this.onClickPlaySound(index)} type="sound" theme="twoTone" />
                  </div>
                  <div className="content-skill-left">
                    <p> {index + 1}. </p>
                  </div>
                  <div className="content-skill-right">
                    <div className="question-skill">
                      <p> {value.question} </p>
                      {
                        value.imagesTop && <img
                          className="magin-img-skill"
                          src={value.imagesTop.src}
                          width={value.imagesTop.width}
                        />
                      }
                      {
                        value.accessories && <p> {value.accessories} </p>
                      }
                      {
                        value.imagesBottom && <img
                          className="magin-img-skill"
                          src={value.imagesBottom.src}
                          width={value.imagesBottom.width}
                        />
                      }
                      {value.note && <p> {value.note} </p>}
                    </div>
                    <div className="content-footer font-small">
                      <div className="icon-youtube">
                        <a href={value.urlVideo}><Icon type="youtube" /> ดูวิดีโอ </a>
                      </div>
                      <div className="icon-info">
                        <ModalInfo index={index} />
                      </div>
                    </div>
                  </div>
                </div>
                <div className="content-no">
                  <div className="content-no-on">
                    <Radio.Group onChange={(event) => this.onChangeRadio(event, index)}>
                      <Radio className="font green-color box-radio padding-buttom-on" value={1} > ผ่าน </Radio>
                      <Radio className="font red-color box-radio" value={0} > ไม่ผ่าน </Radio>
                    </Radio.Group>
                  </div>
                  <div>
                    <Input defaultValue={this.state.content[index].date} className="font" />
                  </div>
                  <div >
                    <ModalAssessor index={index + 1} />
                  </div>
                </div>
              </div>
              <hr className="separator-no" />
            </div>
          )
        })}
      </div>
    )
  }
}
