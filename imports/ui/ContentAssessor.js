import React, { Component } from 'react'
import { Input } from 'antd';

export default class ContentAssessor extends Component {
    render() {
        return (
            <div className="content-assessor">
                <div className="content">
                    <div className="left">
                        <p>ผู้ประเมิน</p>
                    </div>
                    <div className="right">
                        <Input className="font font-small"/>
                    </div>
                </div>
                <div className="content">
                    <div className="left">
                        <p>รหัสโรงพยาบาล</p>
                    </div>
                    <div className="right">
                        <Input className="font font-small"/>
                    </div>
                </div>
            </div>
        )
    }
}