import React, { Component } from 'react'

export default class ContentModalInfo extends Component {
    constructor(props) {
        super()
    }
    state = {
        contentModal: [
            {
                evaluation: {
                    text: ['วางเด็กในท่านอนหงาย สังเกตการเคลื่อนไหวแขนขาของเด็ก'],
                    complete: 'ผ่าน: เด็กสามารถเคลื่อนไหว แขน ขา ได้อย่างสมดุลทั้งสองข้าง'
                },
                practiceSkills: { text: ['ขยับยกแขนขาทั้งสองข้างของเด็กบ่อย ๆ'] }
            },
            {
                evaluation: {
                    text: ['อุ้มเด็กนอนบนตัก พูดคุยกับเด็ก'],
                    complete: 'ผ่าน: เด็กมองสบตาขณะตื่นได้'
                },
                practiceSkills: {
                    text: [
                        'มองสบตาและพูดคุยกับเด็กบ่อยๆ เช่น ขณะให้นมแม'
                    ]
                }
            },
            {
                evaluation: {
                    text: ['1. จัดเด็กในท่านอนหงายขณะที่เด็กตื่นและไม่งอแง', '2. เรียกชื่อเด็กจากด้านข้างทั้งซ้ายและขวาโดยพูดเสียงดังปกติห่างจากเด็กประมาณ 60 ซม.'],
                    complete: 'ผ่าน: เด็กแสดงการรับรู้ด้วยการกระพริบตา สะดุ้ง หรือเคลื่อนไหวร่างกาย'
                },
                practiceSkills: {
                    text: ['1. จัดให้เด็กอยู่ในท่านอนหงาย เรียกชื่อหรือพูดคุยกับเด็กจากด้านข้างซ้าย และขวาด้วยเสียงปกติ',
                        '2. หากเด็กสะดุ้ง หรือขยับตัวเมื่อพูดคุยเสียงดังปกติให้ยิ้ม และสัมผัสตัวเด็ก',
                        '3. ถ้าเด็กไม่มีปฏิกิริยาใดๆ ให้พูดเสียงดังเพิ่มขึ้นโดยจัดท่าเด็กเช่นเดียวกับข้อ1หากเด็กสะดุ้งหรือขยับตัวให้ลดเสียงลงอยู่ในระดับดังปกติพร้อมกับสัมผัสตัวเด็ก']
                }
            },
            {
                evaluation: {
                    text: ['ฟังเสียงเด็กขณะประเมินหรือสอบถามจากผู้ปกครองว่าเด็ก ทำเสียงอะไรได้บ้าง'],
                    complete: 'เด็กสามารถเปล่งเสียงได้ ที่ไม่ใช่เสียงร้องไห้ เช่น แฮะ หรือ แอะ'
                },
                practiceSkills: { text: ['สบตาและพูดคุยกับเด็กบ่อย ๆ'] }
            },
            {
                evaluation: {
                    text: ['ถามพ่อแม่ ผู้ปกครองว่าเด็กดูดนมแม่อย่างสมํ่าเสมอหรือไม่ หรือสังเกตขณะกำลังให้นม'],
                    complete: 'เด็กดูดนมแม่ได้อย่างสมํ่าเสมอ'
                },
                practiceSkills: { text: ['กระตุ้นการดูดกลืนของเด็กโดยการใช้มือลูบเบาๆ ที่แก้มของเด็กก่อนให้นมแม่ทุกครั้ง'] }
            },
        ]
    }

    render() {
        return (
            <div className="content-modal">
                <div className="evaluation">
                    <p className="black-color" >วิธีการประเมิน และเฝ้าระวัง โดยพ่อแม่ ผู้ปกครอง และเจ้าหน้าที่</p>
                    {
                        this.state.contentModal[this.props.index].evaluation.text.map((value, index) => <p key={+index}> {value} </p>)
                    }
                    <p className="blue-color" >ผ่าน: {this.state.contentModal[this.props.index].evaluation.complete} </p>
                </div>
                <div className="practice-skills">
                    <p className="black-color" >วิธีฝึกทักษะ โดยพ่อแม่ ผู้ปกครอง และเจ้าหน้าที่</p>
                    {
                        this.state.contentModal[this.props.index].practiceSkills &&
                        this.state.contentModal[this.props.index].practiceSkills.text.map((value, index) => <p key={+index}> {value} </p>)
                    }
                    {
                        this.state.contentModal[this.props.index].practiceSkills.replacementToys &&
                        <p className="red-color"> {this.state.contentModal[this.props.index].practiceSkills.replacementToys} </p>
                    }
                </div>
            </div>
        )
    }
}