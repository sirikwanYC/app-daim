import React, { Component } from 'react'
import { Router, Route, Switch } from 'react-router';
import createBrowserHistory from 'history/createBrowserHistory';
import Home from '../../ui/pages/Home.js'
import Register from '../../ui/pages/Register'
import Assessment from '../../ui/pages/Assessment.js'

const browserHistory = createBrowserHistory();

export const renderRoutes = () => (
  <Router history={browserHistory}>
    <Switch>
      <Route exact path="/" component={Home} />
      <Route exact path="/register" component={Register} />
      <Route exact path="/assessment" component={Assessment} />
    </Switch>
  </Router>
)